/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.controller;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Test controllers
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    private static final YearMonth CUR_MONTH_YEAR = YearMonth.now(); 
    
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Welcome!")));
    }

    @Test
    public void listIssuers() throws Exception {
        this.mockMvc.perform(get("/operation/issuers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].issuer", is("AMEX")));
    }

    @Test
    public void listCreditCards() throws Exception {
        this.mockMvc.perform(get("/operation/creditcards")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].cardNumber", is("3331-2222-3333-4444")));
    }

    @Test
    public void feeOperationVisa() throws Exception {
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA\",\"amount\":100,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.issuer", is("VISA")))
                .andExpect(jsonPath("$.amount", is(100.0)))
                .andExpect(jsonPath("$.fee", is((CUR_MONTH_YEAR.getYear() % 100D) / Double.valueOf(CUR_MONTH_YEAR.getMonthValue()))));
    }

    @Test
    public void feeOperationAmex() throws Exception {
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"AMEX\",\"amount\":100,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.issuer", is("AMEX")))
                .andExpect(jsonPath("$.amount", is(100.0)))
                .andExpect(jsonPath("$.fee", is(CUR_MONTH_YEAR.getMonthValue() * .1D)));
    }

    @Test
    public void feeOperationNara() throws Exception {
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"NARA\",\"amount\":100,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.issuer", is("NARA")))
                .andExpect(jsonPath("$.amount", is(100.0)))
                .andExpect(jsonPath("$.fee", is(0.5D)));
    }

    @Test
    public void feeOperationInvalid() throws Exception {
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA\",\"amount\":0,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("Amount must be greater than '0.00'")));
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA\",\"amount\":-100.5,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("Amount must be greater than '0.00'")));
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA1\",\"amount\":100.5,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("Issuer name not found")));
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA\",\"amount\":1000,\"expiration\":\"" + CUR_MONTH_YEAR.format(DateTimeFormatter.ofPattern("MM/uu")) + "\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("Amount must be less than '1000.00'")));
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA\",\"amount\":900,\"expiration\":\"08/23\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("Expiration date exceeded")));
        this.mockMvc.perform(post("/operation/fee")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issuer\":\"VISA\",\"amount\":900,\"expiration\":\"xxxxxxxx\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("Failed to parse expiration date with format 'MM/uu'")));
    }

}
