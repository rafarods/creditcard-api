package com.eldar.challenge.creditcard;

import com.eldar.challenge.creditcard.model.CreditCardFeeResponse;
import com.eldar.challenge.creditcard.model.CreditCardFeeRequest;
import com.eldar.challenge.creditcard.model.Utils;
import com.eldar.challenge.creditcard.service.CreditCardService;
import com.eldar.challenge.creditcard.service.IssuerService;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Assertions;

/**
 * Test application
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@SpringBootTest
public class CreditcardApplicationTests {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private IssuerService issuerService;

    @Test
    public void listIssuer() {
        Assertions.assertFalse(issuerService.fetchAll().isEmpty());
    }

    @Test
    public void listCreditCard() {
        Assertions.assertFalse(creditCardService.fetchAll().isEmpty());
    }

    @Test
    public void testCreditCardHolderName() {
        Assertions.assertEquals("MILENA GUTIERREZ", creditCardService.fetchByCardNumber("3331-2222-3333-4444").getHolderName());
    }

    @Test
    public void testGetFee() {
        creditCardService.getFeeOperation(new CreditCardFeeRequest("visa", "09/26", 100D));
        Assertions.assertEquals(new CreditCardFeeResponse("VISA", 100D, 26D / 9D), creditCardService.getFeeOperation(new CreditCardFeeRequest("VISA", "09/26", 100D)));
    }
}
