/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.model;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Endpoint data for credit card
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCardResponse {

    /**
     * Issuer object
     */
    private String issuer;

    /**
     * Issuer name
     */
    private String cardNumber;

    /**
     * Holder name
     */
    private String holderName;

    /**
     * Card expiration date
     */
    private YearMonth expiration;

    /**
     * Get Expiration in format MM/uu
     *
     * @return Month and year
     */
    public String getExpiration() {
        return expiration.format(DateTimeFormatter.ofPattern("MM/uu"));
    }
}
