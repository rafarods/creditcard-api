/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.model;

/**
 * Utility class
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class Utils {

    /**
     * Round double
     *
     * @param value Amount
     * @param decimalPlaces Number of decimals
     * @return Rounded amount
     */
    public static double round(final double value, final int decimalPlaces) {
        final double factor = Math.pow(10, decimalPlaces);
        return Math.round(value * factor) / factor;
    }

    /**
     * Round double to 2 decimals
     *
     * @param value Amount
     * @return Rounded amount
     */
    public static double round(final double value) {
        return round(value, 2);
    }
}
