/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.model;

import com.eldar.challenge.creditcard.exception.YearMonthFormatException;
import com.eldar.challenge.creditcard.validator.Fee;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Endpoint card fee request
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Fee(minAmount = 0D, inclusiveAmount = false, expirationFormat = "MM/uu")
public class CreditCardFeeRequest {

    /**
     * Issuer name
     */
    @NotBlank(message = "{com.eldar.validation.message.issuer}")
    private String issuer;

    /**
     * Expiration date in format MM/yy
     */
    @NotBlank(message = "{com.eldar.validation.message.expiration.notblank}")
    private String expiration;

    /**
     * Amount
     */
    @NotNull(message = "{com.eldar.validation.message.amount}")
    private Double amount;

    /**
     * Get expiration as object date, format allowed is MM/[uu][uuuu]
     *
     * @return Object date
     */
    public YearMonth getExpirationAsYearMonth() {
        try {
            return YearMonth.parse(this.expiration, DateTimeFormatter.ofPattern("MM/uu"));
        } catch (DateTimeParseException ex) {
            throw new YearMonthFormatException(ex.getMessage());
        }
    }
}
