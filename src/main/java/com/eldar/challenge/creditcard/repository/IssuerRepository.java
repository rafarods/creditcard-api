/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.repository;

import com.eldar.challenge.creditcard.entity.Issuer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Issuer repository
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public interface IssuerRepository extends JpaRepository<Issuer, Long> {

    /**
     * Find one issuer by name
     *
     * @param name Name
     * @return Issuer entity
     */
    public Issuer findFirstByName(final String name);

}
