/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.converter;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Converter(autoApply = true)
@Slf4j
public class YearMonthConverter implements AttributeConverter<YearMonth, String> {

    /**
     *
     * @param yearMonth
     * @return
     */
    @Override
    public String convertToDatabaseColumn(final YearMonth yearMonth) {
        return yearMonth.toString();
    }

    /**
     *
     * @param yearMonth
     * @return
     */
    @Override
    public YearMonth convertToEntityAttribute(final String yearMonth) {
        try {
            return YearMonth.parse(yearMonth, DateTimeFormatter.ofPattern("uuuu-MM"));
        } catch (DateTimeParseException ex) {
            log.error("Error Year-Month converter", ex);
            return YearMonth.now().plusMonths(1);
        }
    }
}
