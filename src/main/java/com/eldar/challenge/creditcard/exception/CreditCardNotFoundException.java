/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.exception;

/**
 * Credit card not found
 * 
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class CreditCardNotFoundException extends RuntimeException  {

    /**
     * Serial ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor
     * 
     * @param string Error message
     */
    public CreditCardNotFoundException(String string) {
        super(string);
    }

}
