/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.controller;

import com.eldar.challenge.creditcard.exception.AmountExceededException;
import com.eldar.challenge.creditcard.exception.CreditCardExpiredException;
import com.eldar.challenge.creditcard.exception.CreditCardNotFoundException;
import com.eldar.challenge.creditcard.exception.InvalidAmountException;
import com.eldar.challenge.creditcard.exception.IssuerNotFoundException;
import com.eldar.challenge.creditcard.exception.YearMonthFormatException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Error handler controller advice
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@ControllerAdvice
public class ErrorHandlerController extends ResponseEntityExceptionHandler {

    /**
     * Handler for requested validations
     *
     * @param ex Exception object
     * @param headers HTTP Headers
     * @param status HTTP status
     * @param request Request
     * @return JSON array with errors found
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request
    ) {
        final Map<String, List<String>> body = new HashMap<>();
        final List<String> errors = ex.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        body.put("errors", errors);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    /**
     * Credit card not found
     *
     * @param ex Exception object
     * @param request Request
     * @return Error message
     */
    @ExceptionHandler(value = {CreditCardNotFoundException.class})
    protected ResponseEntity<Object> handleCreditCardNotFound(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<Object>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Invalid amount
     *
     * @param ex Exception object
     * @param request Request
     * @return Error message
     */
    @ExceptionHandler(value = {InvalidAmountException.class})
    protected ResponseEntity<Object> handleInvalidAmount(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<Object>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Amount exceeded
     *
     * @param ex Exception object
     * @param request Request
     * @return Error message
     */
    @ExceptionHandler(value = {AmountExceededException.class})
    protected ResponseEntity<Object> handleAmountExceeded(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<Object>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Year/month format exception
     *
     * @param ex Exception object
     * @param request Request
     * @return Error message
     */
    @ExceptionHandler(value = {YearMonthFormatException.class})
    protected ResponseEntity<Object> handleYearMonthFormatException(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<Object>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Credit card expired
     *
     * @param ex Exception object
     * @param request Request
     * @return Error message
     */
    @ExceptionHandler(value = {CreditCardExpiredException.class})
    protected ResponseEntity<Object> handleCreditCardExpirated(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<Object>("Credit card is expired", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Credit card not found
     *
     * @param ex Exception object
     * @param request Request
     * @return Error message
     */
    @ExceptionHandler(value = {IssuerNotFoundException.class})
    protected ResponseEntity<Object> handleIssuerNotFound(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<Object>("Issuer not found", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
