/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.controller;

import com.eldar.challenge.creditcard.model.CreditCardFeeResponse;
import com.eldar.challenge.creditcard.model.CreditCardFeeRequest;
import com.eldar.challenge.creditcard.model.CreditCardResponse;
import com.eldar.challenge.creditcard.model.IssuerResponse;
import com.eldar.challenge.creditcard.service.CreditCardService;
import com.eldar.challenge.creditcard.service.IssuerService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller API operations
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@RestController
@RequestMapping("/operation")
public class OperationController {

    /**
     * Service credit card
     */
    @Autowired
    private CreditCardService creditCardService;

    /**
     * Service issuer
     */
    @Autowired
    private IssuerService issuerService;

    /**
     * Get fee amount in JSON format
     *
     * @param ccRequest
     * @return Entity JSON
     */
    @PostMapping("/fee")
    public ResponseEntity<CreditCardFeeResponse> feeOperation(@Valid @RequestBody CreditCardFeeRequest ccRequest) {
        final CreditCardFeeResponse response = creditCardService.getFeeOperation(ccRequest);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    /**
     * Get all credit cards
     *
     * @return Entity JSON
     */
    @GetMapping("/creditcards")
    public ResponseEntity<List<CreditCardResponse>> listCreditCard() {
        return ResponseEntity.status(HttpStatus.OK).body(creditCardService.fetchAll());
    }

    /**
     * Get all issuers
     *
     * @return Entity JSON
     */
    @GetMapping("/issuers")
    public ResponseEntity<List<IssuerResponse>> listIssuer() {
        return ResponseEntity.status(HttpStatus.OK).body(issuerService.fetchAll());
    }
}
