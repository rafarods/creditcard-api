/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.validator;

import com.eldar.challenge.creditcard.entity.Issuer;
import com.eldar.challenge.creditcard.exception.IssuerNotFoundException;
import com.eldar.challenge.creditcard.model.CreditCardFeeRequest;
import com.eldar.challenge.creditcard.service.IssuerService;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Validation for amount
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Slf4j
public class FeeValidator implements ConstraintValidator<Fee, CreditCardFeeRequest> {

    /**
     * Issuer service
     */
    @Autowired
    private IssuerService issuerService;

    /**
     * List of errors
     */
    private final Map<String, String> erMsgs = new HashMap<>();

    /**
     * Expiration format
     */
    private String expirationFormat;

    /**
     * Min value
     */
    private double minAmount;

    /**
     * Inclusive range?
     */
    private boolean inclusiveAmount;

    /**
     * Init values
     *
     * @param fee Fee values
     */
    @Override
    public void initialize(Fee fee) {
        this.minAmount = fee.minAmount();
        this.inclusiveAmount = fee.inclusiveAmount();
        this.expirationFormat = fee.expirationFormat();
    }

    /**
     * Validate expiration date
     *
     * @param expiration Date expiration
     */
    private void validateExpiration(final String expiration) {
        try {
            final YearMonth ymExp = YearMonth.parse(expiration, DateTimeFormatter.ofPattern(this.expirationFormat));
            if (YearMonth.now().isAfter(ymExp)) {
                this.erMsgs.put("expiration", "{com.eldar.validation.message.expiration.exceeded}");
            }
        } catch (DateTimeParseException ex) {
            this.erMsgs.put("expiration", "{com.eldar.validation.message.expiration.parse}");
        }
    }

    /**
     * Validate amount by issuer name
     *
     * @param issuerName Issuer name
     * @param amount Amount value
     */
    private void validateAmount(final String issuerName, final Double amount) {
        try {
            final Issuer issuer = this.issuerService.fetchByName(issuerName);
            if (this.inclusiveAmount) {
                if (amount < this.minAmount) {
                    this.erMsgs.put("amount", String.format("Amount must be greater than or equal than '%.2f'", this.minAmount));
                } else if (amount > issuer.getMaxAmount()) {
                    this.erMsgs.put("amount", String.format("Amount must be less or equal than '%.2f'", issuer.getMaxAmount().doubleValue()));
                }
            } else {
                if (amount <= this.minAmount) {
                    this.erMsgs.put("amount", String.format("Amount must be greater than '%.2f'", this.minAmount));
                } else if (amount >= issuer.getMaxAmount()) {
                    this.erMsgs.put("amount", String.format("Amount must be less than '%.2f'", issuer.getMaxAmount().doubleValue()));
                }
            }
        } catch (IssuerNotFoundException ex) {
            this.erMsgs.put("issuer", "{com.eldar.validation.message.issuer.notfound}");
        }
    }

    /**
     * Is valid?
     *
     * @param ccFee Fee request to validate
     * @param context Context
     * @return True or false
     */
    @Override
    public boolean isValid(final CreditCardFeeRequest ccFee, final ConstraintValidatorContext context) {
        this.erMsgs.clear();
        if (ccFee != null && ccFee.getAmount() != null
                && ccFee.getExpiration() != null && !ccFee.getExpiration().trim().isEmpty()
                && ccFee.getIssuer() != null && !ccFee.getIssuer().trim().isEmpty()) {
            this.validateAmount(ccFee.getIssuer(), ccFee.getAmount());
            this.validateExpiration(ccFee.getExpiration());
            this.erMsgs.forEach((String fieldName, String template) -> {
                context.buildConstraintViolationWithTemplate(template)
                        .addPropertyNode(fieldName)
                        .addConstraintViolation();
            });
        }
        return this.erMsgs.isEmpty();
    }
}
