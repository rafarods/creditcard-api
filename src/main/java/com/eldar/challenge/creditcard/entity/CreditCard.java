/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.entity;

import java.time.YearMonth;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Credit Card entity
 * 
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Entity
@Data
public class CreditCard {

    /**
     * CreditCard ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    private long id;

    /**
     * Issuer object
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "issuer_id", referencedColumnName = "id")
    private Issuer issuer;

    /**
     * Issuer name
     */
    @Column(nullable = false, length = 31, unique = true)
    @Size(min = 16, max = 31)
    @NotNull
    private String cardNumber;

    /**
     * Holder name
     */
    @Column(nullable = false, length = 31)
    @Size(min = 3, max = 31)
    @NotNull
    private String holderName;

    /**
     * Card expiration date
     */
    @NotNull
    private YearMonth expiration;
}
