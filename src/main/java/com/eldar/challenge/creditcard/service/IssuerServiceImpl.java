/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.service;

import com.eldar.challenge.creditcard.entity.Issuer;
import com.eldar.challenge.creditcard.exception.IssuerNotFoundException;
import com.eldar.challenge.creditcard.model.IssuerResponse;
import com.eldar.challenge.creditcard.repository.IssuerRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Service
public class IssuerServiceImpl implements IssuerService {

    /**
     * Model mapper from entity to DTO
     */
    private final ModelMapper modelMapper = new ModelMapper();

    /**
     * Issuer repository
     */
    @Autowired
    private IssuerRepository issuerRepository;

    /**
     * Default constructor
     */
    public IssuerServiceImpl() {
        // Mapper config
        modelMapper.createTypeMap(Issuer.class, IssuerResponse.class)
                .addMappings(mapper -> {
                    mapper.map(src -> src.getName(), IssuerResponse::setIssuer);
                });
    }

    /**
     * Fetch all issuers
     *
     * @return Issuer object
     */
    @Override
    public List<IssuerResponse> fetchAll() {
        final List<IssuerResponse> resp = new ArrayList<>(3);
        issuerRepository.findAll(Sort.by("name")).forEach((Issuer issuer) -> {
            resp.add(modelMapper.map(issuer, IssuerResponse.class));
        });
        return resp;
    }

    /**
     * Fetch issuer by name
     *
     * @param name Issuer name (is case insensitive)
     * @return Issuer entity
     * @throws IssuerNotFoundException When not found
     */
    @Override
    public Issuer fetchByName(final String name) throws NoSuchElementException {
        final Issuer issuer = issuerRepository.findFirstByName(name.trim().toUpperCase());
        if (issuer == null) {
            throw new IssuerNotFoundException("Issuer name '" + name + "' not found");
        }
        return issuer;
    }

}
