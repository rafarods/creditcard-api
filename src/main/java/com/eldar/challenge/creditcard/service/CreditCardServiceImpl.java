/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.service;

import com.eldar.challenge.creditcard.entity.CreditCard;
import com.eldar.challenge.creditcard.entity.Issuer;
import com.eldar.challenge.creditcard.exception.AmountExceededException;
import com.eldar.challenge.creditcard.exception.CreditCardExpiredException;
import com.eldar.challenge.creditcard.exception.CreditCardNotFoundException;
import com.eldar.challenge.creditcard.exception.FormulaException;
import com.eldar.challenge.creditcard.exception.InvalidAmountException;
import com.eldar.challenge.creditcard.exception.IssuerNotFoundException;
import com.eldar.challenge.creditcard.model.CreditCardFeeResponse;
import com.eldar.challenge.creditcard.model.CreditCardFeeRequest;
import com.eldar.challenge.creditcard.model.CreditCardResponse;
import com.eldar.challenge.creditcard.model.Utils;
import com.eldar.challenge.creditcard.repository.CreditCardRepository;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Service implementation for Credit Card
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Service
@Slf4j
public class CreditCardServiceImpl implements CreditCardService {

    /**
     * Model mapper from entity to DTO
     */
    private final ModelMapper modelMapper = new ModelMapper();

    /**
     * JavaScript engine for formula evaluations
     */
    final ScriptEngine scriptEngine = (new ScriptEngineManager()).getEngineByName("JavaScript");

    /**
     * Credit card repository
     */
    @Autowired
    private CreditCardRepository creditCardRepository;

    /**
     * Issuer service
     */
    @Autowired
    private IssuerService issuerService;

    /**
     * Default constructor
     */
    public CreditCardServiceImpl() {
        // Mapper config
        modelMapper.createTypeMap(CreditCard.class, CreditCardResponse.class)
                .addMappings(mapper -> {
                    mapper.map(src -> src.getIssuer().getName(), CreditCardResponse::setIssuer);
                });
    }

    /**
     * Fetch credit card by number
     *
     * @param cardNumber Credit card number
     * @return Credit card founded
     * @throws CreditCardNotFoundException
     */
    @Override
    public CreditCard fetchByCardNumber(final String cardNumber) {
        final CreditCard card = creditCardRepository.findFirstByCardNumber(cardNumber);
        if (card == null) {
            throw new CreditCardNotFoundException("Credit card number '" + cardNumber + "' not found");
        }
        return card;
    }

    /**
     * Get all credit card
     *
     * @return List of credit card
     */
    @Override
    public List<CreditCardResponse> fetchAll() {
        final List<CreditCardResponse> cardResp = new ArrayList<>();
        creditCardRepository.findAll(Sort.by("issuer.name").and(Sort.by("cardNumber")))
                .forEach((CreditCard card) -> {
                    cardResp.add(modelMapper.map(card, CreditCardResponse.class));
                });
        return cardResp;
    }

    /**
     *
     * Get fee calculation
     *
     * @param cardReq Object with values
     * @return DTO object with the value
     * @throws FormulaException
     * @throws IssuerNotFoundException
     */
    @Override
    public CreditCardFeeResponse getFeeOperation(final CreditCardFeeRequest cardReq) {
        final YearMonth expYM = cardReq.getExpirationAsYearMonth();
        final Issuer issuer = issuerService.fetchByName(cardReq.getIssuer());
        this.validate(cardReq.getAmount(), issuer.getMaxAmount(), expYM);
        final Bindings bindings = new SimpleBindings();
        bindings.put("day", 1);
        bindings.put("month", expYM.getMonthValue());
        // Only take the two last digits 
        bindings.put("year", expYM.getYear() % 100D);
        final CreditCardFeeResponse cardFeeResp = new CreditCardFeeResponse();
        cardFeeResp.setIssuer(issuer.getName());
        cardFeeResp.setAmount(cardReq.getAmount());
        try {
            cardFeeResp.setFee(Double.parseDouble(this.scriptEngine.eval(issuer.getFormula(), bindings).toString()));
        } catch (ScriptException ex) {
            throw new FormulaException("Failed in fee formula '" + issuer.getFormula() + "'");
        }
        return cardFeeResp;
    }

    /**
     * Validate transaction
     *
     * @param amount Amount to evaluate
     * @param maxAmount Max. amount by issuer
     * @param expiration Expiration date
     * @throws InvalidAmountException
     * @throws AmountExceededException
     * @throws CreditCardExpiredException
     */
    private void validate(final double amount, final double maxAmount, final YearMonth expiration) {
        if (amount < 0) {
            throw new InvalidAmountException("Negative amounts are not permitted");
        }
        if (Utils.round(amount, 2) == 0D) {
            throw new InvalidAmountException("Zero amount is not permitted");
        }
        if (Utils.round(amount, 2) >= maxAmount) {
            throw new AmountExceededException("Maximum amount exceeded");
        }
        if (YearMonth.now().isAfter(expiration)) {
            throw new CreditCardExpiredException("Credit card is expired");
        }
    }

}
