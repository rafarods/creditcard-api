/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.service;

import com.eldar.challenge.creditcard.entity.Issuer;
import com.eldar.challenge.creditcard.exception.IssuerNotFoundException;
import com.eldar.challenge.creditcard.model.IssuerResponse;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Issuer service
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public interface IssuerService {

    /**
     * Fetch all issuers
     *
     * @return Issuer object
     */
    public List<IssuerResponse> fetchAll();

    /**
     * Fetch issuer by name
     *
     * @param name Issuer name (is case insensitive)
     * @return Issuer entity
     * @throws IssuerNotFoundException When not found
     */
    public Issuer fetchByName(final String name) throws NoSuchElementException;
}
