/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.challenge.creditcard.service;

import com.eldar.challenge.creditcard.entity.CreditCard;
import com.eldar.challenge.creditcard.exception.FormulaException;
import com.eldar.challenge.creditcard.exception.IssuerNotFoundException;
import com.eldar.challenge.creditcard.model.CreditCardFeeResponse;
import com.eldar.challenge.creditcard.model.CreditCardFeeRequest;
import com.eldar.challenge.creditcard.model.CreditCardResponse;
import java.util.List;

/**
 * Credit card service
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public interface CreditCardService {

    /**
     * Fetch credit card by number
     *
     * @param cardNumber Credit card number
     * @return Credit card founded
     */
    public CreditCard fetchByCardNumber(final String cardNumber);

    /**
     * Get all credit card
     *
     * @return List of credit card
     */
    public List<CreditCardResponse> fetchAll();

    /**
     *
     * Get fee calculation
     *
     * @param cardReq Object with values
     * @return DTO object with the value
     * @throws FormulaException
     * @throws IssuerNotFoundException
     */
    public CreditCardFeeResponse getFeeOperation(final CreditCardFeeRequest cardReq);
}
