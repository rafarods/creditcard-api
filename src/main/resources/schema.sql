/* 
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Author:  Rafael Rodriguez <rafarods@outlook.com>
 * Created: Sep 20, 2023
 */
DROP TABLE IF EXISTS transact;
DROP TABLE IF EXISTS credit_card;
DROP TABLE IF EXISTS issuer;

CREATE TABLE issuer (
    id INT AUTO_INCREMENT,
    name VARCHAR(31) NOT NULL,
    formula VARCHAR(255) NOT NULL,
    max_amount INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE credit_card (
    id INT AUTO_INCREMENT,
    issuer_id INT NOT NULL,
    card_number VARCHAR(31) NOT NULL,
    holder_name VARCHAR(31) NOT NULL,
    expiration CHAR(7) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (issuer_id) REFERENCES issuer (id),
    UNIQUE (card_number)
);

CREATE TABLE transact (
    id INT AUTO_INCREMENT,
    issuer VARCHAR(31) NOT NULL,
    card_number VARCHAR(31) NOT NULL,
    expiration CHAR(7) NOT NULL,
    amount FLOAT NOT NULL
);