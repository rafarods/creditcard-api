/* 
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Author:  Rafael Rodriguez <rafarods@outlook.com>
 * Created: Sep 20, 2023
 */
INSERT INTO issuer (name, formula, max_amount) VALUES
    ('VISA', 'year / month', 1000),
    ('NARA', 'day * 0.5', 1000),
    ('AMEX', 'month * 0.1', 1000);

INSERT INTO credit_card (issuer_id, card_number, holder_name, expiration) VALUES
    (1, '1111-2222-3333-4444', 'RAFAEL RODRIGUEZ', '2026-01'),
    (1, '1112-2223-3334-4445', 'JUAN PEREZ', '2026-02'),
    (1, '1113-2224-3335-4446', 'LUIS GARCIA', '2026-03'),
    (2, '2221-2222-3333-4444', 'PEDRO MARTINEZ', '2026-04'),
    (2, '2222-2223-3334-4445', 'MIGUEL ARIAS', '2026-05'),
    (2, '2223-2224-3335-4446', 'LORENZO SALAS', '2026-06'),
    (3, '3331-2222-3333-4444', 'MILENA GUTIERREZ', '2026-07'),
    (3, '3332-2223-3334-4445', 'JIMENA SALAZAR', '2026-08'),
    (3, '3333-2224-3335-4446', 'MARIA HERNANDEZ', '2026-09');