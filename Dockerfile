# Docker file for Ubuntu with OpenJDK 8 and Tomcat 9.
FROM ubuntu:latest
LABEL maintainer="Rafael Rodriguez <rafarods@outlook.com>"

# Set environment variables
ENV TOMCAT_VERSION 9.0.80
ENV CATALINA_HOME /usr/local/tomcat
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV PATH $CATALINA_HOME/bin:$PATH

# Install JDK & wget packages.
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install openjdk-8-jdk wget libapr1

# Install and configure Tomcat.
RUN mkdir $CATALINA_HOME
RUN wget https://archive.apache.org/dist/tomcat/tomcat-9/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -O /tmp/tomcat.tar.gz
RUN cd /tmp && tar xvfz tomcat.tar.gz
RUN rm -rf /tmp/apache-tomcat-${TOMCAT_VERSION}/webapps/docs /tmp/apache-tomcat-${TOMCAT_VERSION}/webapps/examples /tmp/apache-tomcat-${TOMCAT_VERSION}/webapps/host-manager
RUN cp -Rv /tmp/apache-tomcat-${TOMCAT_VERSION}/* $CATALINA_HOME
RUN rm -rf /tmp/apache-tomcat-${TOMCAT_VERSION}
RUN rm -rf /tmp/tomcat.tar.gz
RUN rm -rf $CATALINA_HOME/conf/tomcat-users.xml
RUN echo '<?xml version="1.0" encoding="UTF-8"?><tomcat-users><role rolename="manager-gui"/><user password="admin" roles="manager-gui,manager-script" username="admin"/></tomcat-users>' >> $CATALINA_HOME/conf/tomcat-users.xml
RUN cat $CATALINA_HOME/conf/tomcat-users.xml
RUN rm -rf $CATALINA_HOME/webapps/manager/META-INF/context.xml
RUN echo '<?xml version="1.0" encoding="UTF-8"?><Context antiResourceLocking="false" privileged="true"><Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$"/></Context>' >> $CATALINA_HOME/webapps/manager/META-INF/context.xml
RUN cat $CATALINA_HOME/webapps/manager/META-INF/context.xml
RUN wget https://gitlab.com/rafarods/creditcard-api/-/raw/main/target/creditcard-api__0.0.1-SNAPSHOT.war?ref_type=heads -O $CATALINA_HOME/webapps/creditcard-api.war

# Expose Tomcat port.
EXPOSE 80

# Start Tomcat
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]
